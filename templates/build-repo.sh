#!/bin/sh

[ $USER -eq "{{ repo_builder_user }}" ] || exit -1

cd

BUILDDIR="/tmp/makepkg"
PKGDEST="{{ repo_output_directory }}"

env

mkdir "$BUILDDIR"
echo $?

for d in "{{ repo_checkout_directory }}pkgbuilds"/*; do
	real_d=$(realpath $d)
	echo $real_d
	cd $real_d
	makepkg -sr
	cd
done
